# Escapy project [design document \[rus\]](https://bitbucket.org/tinder-samurai/escapy-des-doc/src/master/desdoc.pdf)
   
   * [**Escapy engine.**](https://bitbucket.org/tinder-samurai/escapy2.0/src/master/)
   * [**Changelog**](https://bitbucket.org/tinder-samurai/escapy-des-doc/src/master/CHANGELOG.md)
   * [**Download**](https://bitbucket.org/tinder-samurai/escapy-des-doc/raw/154fa27193efba68f16973e6179a68a8a6c7d6cb/desdoc.pdf)



## Before compile 
> 1. [**Download**](https://drive.google.com/open?id=0Bx5mBLamQF7HQmprVnM5NEl1bGM) related archive with .eps images.
>   1. Extract it into **`/img/eps`** in your project root.
